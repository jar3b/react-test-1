const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'js/[name]-[hash].js',
    publicPath: "/"
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    }
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      'components': path.resolve(__dirname, '../src/components'),
      'containers': path.resolve(__dirname, '../src/containers'),
      'interfaces': path.resolve(__dirname, '../src/interfaces'),
      'styles': path.resolve(__dirname, '../src/styles'),
      'store': path.resolve(__dirname, '../src/store'),
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader'
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
          }
        ],
      },
      {
        test: /\.css/,
        use: [
          'style-loader',
          'css-loader',
        ]
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
  ],
};
