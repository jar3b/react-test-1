import React from 'react';
import ListContainer from 'containers/ListContainer';
import './styles/base.scss';

export default function App() {
  return <ListContainer />;
}
