import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { loadState } from './localStorageHelpers';
import rootReducer from './reducers/rootReducer';
import rootSaga from './sagas/rootSaga';

declare global {
  interface Window { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any; }
}

// Load state from localstorage
const persistedState = loadState();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, persistedState, composeEnhancers(
  applyMiddleware(sagaMiddleware),
));

sagaMiddleware.run(rootSaga);

export default store;
