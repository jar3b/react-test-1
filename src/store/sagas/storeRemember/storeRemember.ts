import { takeEvery, select } from 'redux-saga/effects';
import {
  LIST_ITEM_COMPLETE,
  LIST_ITEM_APPEND,
  LIST_ITEM_EDIT,
  LIST_ITEM_DELETE,
  LIST_AMOUNT_SET,
} from 'store/constants/list';
import { getList } from 'store/selectors/list/getList';
import { loadState, saveState } from 'store/localStorageHelpers';

enum EKeys { List = 'list' }

function* localStorageListSaveSaga() {
  const list = yield select(getList);
  const persisting = loadState() || {};

  saveState({ ...persisting, [EKeys.List]: list });
}

export default [
  takeEvery([
    LIST_ITEM_COMPLETE,
    LIST_ITEM_APPEND,
    LIST_ITEM_EDIT,
    LIST_ITEM_DELETE,
    LIST_AMOUNT_SET,
  ], localStorageListSaveSaga),
];
