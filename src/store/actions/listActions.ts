import {
  LIST_ITEM_DELETE,
  LIST_ITEM_EDIT,
  LIST_ITEM_SET,
  LIST_ITEM_COMPLETE,
  LIST_ITEM_APPEND,
  LIST_AMOUNT_SET,
} from 'store/constants/list';
import { IItem } from 'interfaces/store/list/store.list.interface';

export const setItem = (name: string) => ({
  name,
  type: LIST_ITEM_SET,
});

export const editItem = (id: number, name: string) => ({
  name,
  id,
  type: LIST_ITEM_EDIT,
});

export const deleteItem = (id: number) => ({
  id,
  type: LIST_ITEM_DELETE,
});

export const completeItem = (id: number) => ({
  id,
  type: LIST_ITEM_COMPLETE,
});

export const setAmount = (amount: number) => ({
  amount,
  type: LIST_AMOUNT_SET,
});

export const appendItem = (item: IItem) => ({
  item,
  type: LIST_ITEM_APPEND,
});
