import React, { FC, useState, useCallback, SyntheticEvent, memo } from 'react';
import Input from '../UI/Input';
import Button from '../UI/Button';
import styles from './InputWithButton.scss';

interface IInputWithButtonProps {
  onSubmit: (input: string) => void;
  className?: string;
}

const InputWithButton: FC<IInputWithButtonProps> = ({
  onSubmit: submit,
  className,
}) => {
  const [value, setValue] = useState('');

  const handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault();

    setValue('');
    submit(value);
  };

  const handleInput = useCallback((event: SyntheticEvent) => {
    event.preventDefault();

    const { value } = event.target as HTMLInputElement;

    setValue(value);
  }, [value]);

  return (
    <form className={className} onSubmit={handleSubmit}>
      <Input
        required={true}
        value={value}
        onChange={handleInput}
        placeholder="событие"
      />
      <Button className={styles.button}>Добавить</Button>
    </form>
  );
};

export default memo(InputWithButton);
