export interface IItem {
  name: string;
  id: number;
  completed: boolean;
}

export interface IListStore {
  items: IItem[];
  amount: number;
}
